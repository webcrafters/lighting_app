var API_URL = "http://www.ledtrade.in/index.php/";
//var API_URL = "http://lighting.localhost/index.php/";
                var category_selected = "";
                var selected_product = "";
                var search_product = "";
                var my_cart = [];
                var user_id = 0;
                var selected_ordered = "";

                angular.module('lighting', ['ionic'])
                        .config(function ($stateProvider, $urlRouterProvider) {
    
                            $stateProvider
                                    .state('signin', {
                                        url: '/sign-in',
                                        templateUrl: 'templates/sign-in.html',
                                        controller: 'SignInCtrl'
                                    })
                                    .state('register', {
                                        url: '/register',
                                        templateUrl: 'templates/register.html',
                                        controller: 'RegisterCtrl'
                                    })
                                    
                                    .state('clearSearch', {
                                        url: '/home',
                                        templateUrl: 'templates/home.html',
                                        controller: 'ClearCtrl'
                                    })
                                    .state('productSearch', {
                                        url: '/home',
                                        templateUrl: 'templates/products.html'
                                    })
                                  
                                    .state('tabs.facts', {
                                        url: '/facts',                                        
                                        views: {
                                            'home-tab': {
                                                templateUrl: 'templates/home.html'

                                            }
                                        }
                                    })                                    
                                    .state('tabs', {
                                        url: '/tab',                                        
                                        abstract: true,
                                        templateUrl: 'templates/tabs.html',
                                        
                                    })
                                    .state('tabs.home', { 
                                        url: '/home',    
                                        views: {
                                            'home-tab': {                                                
                                                templateUrl: 'templates/home.html',
                                                controller: 'HomeTabCtrl',                                              
                                                cache: false
                                                
                                            }
                                        }
                                    })
                                   
                                    .state('tabs.products', {                                        
                                        url: '/products',                                      
                                        views: {
                                            'home-tab': {
                                                templateUrl: 'templates/products.html',
                                                controller: 'ProductsCtrl'
                                                
                                            }
                                        }
                                    })
                                    .state('tabs.productdetails', {
                                        url: '/productdetails',                                        
                                        views: {
                                            'home-tab': {
                                                templateUrl: 'templates/productdetails.html',
                                                controller: 'ProductDetailsCtrl'

                                            }
                                        }
                                    })
                                    .state('tabs.mycart', {
                                        url: '/mycart',                                                                               
                                        views: {
                                            'mycart-tab': {
                                                templateUrl: 'templates/mycart.html',
                                                controller: 'MyCartCtrl',
                                                
                                            }
                                        }
                                    })
                                    .state('tabs.myaccount', {
                                        url: '/myaccount',                                         
                                        views: {
                                            'myaccount-tab': {
                                                templateUrl: 'templates/myaccount.html',
                                               
                                            }
                                        }
                                    })
                                     .state('tabs.trackorder', {
                                        url: '/trackorder',                                        
                                        views: {
                                            'myaccount-tab': {
                                                templateUrl: 'templates/trackorder.html',
                                                controller: 'TrackOrderCtrl',
                                                cache: false
                                                
                                            }
                                        }
                                    })
                                     .state('tabs.orderdetail', {
                                        url: '/orderdetail',                                          
                                        views: {
                                            'myaccount-tab': {
                                                templateUrl: 'templates/orderdetail.html',
                                                controller: 'OrderdetailCtrl',
                                              
                                            }
                                        }
                                    })
                                      .state('tabs.terms', {
                                        url: '/terms',
                                        views: {
                                            'myaccount-tab': {
                                                templateUrl: 'templates/terms.html'
                                            }
                                        }
                                    })
                                     .state('tabs.privacy', {
                                        url: '/privacy',
                                        views: {
                                            'myaccount-tab': {
                                                templateUrl: 'templates/privacy.html'
                                            }
                                        }
                                    })
                                
                                   
                            
                            $urlRouterProvider.otherwise('/sign-in');

                        })

                        .controller('SignInCtrl', function ($scope, $state) {
                            $scope.user = {"username": "deepan", "password": "abcd1234"};
                            $scope.signIn = function (user) {
                                console.log('Sign-In', user);
                                if (user == undefined) {
                                    $(".login-error").show();
                                } else {
                                    if (user.username == undefined || user.username == "" || user.password == undefined || user.password == "") {
                                        $(".login-error").show();
                                    } else {
                                        $.getJSON(API_URL + "user-check?username=" + user.username + "&password=" + user.password, function (data) {
                                            console.log(data);                                          
                                            if (data.status == true) {                                              
                                                user_id = data.message.auth;
                                                $state.go('tabs.home');
                                            } else {
                                                $(".login-error").show();
                                            }
                                        });
                                    }
                                }
                            };
                        })

                        .controller('RegisterCtrl', function ($scope, $state) {

                            $scope.register = function (newuser) {
                                console.log('Register', newuser);
                                if (newuser == undefined) {
                                    alert("Fields are required");
                                } else {
                                    if (newuser.username == undefined || newuser.username == "" || newuser.email == undefined || newuser.email == "" || newuser.mobile == undefined || newuser.password == undefined || newuser.password == "" || newuser.confirmpassword == undefined || newuser.confirmpassword == "" || newuser.name == undefined || newuser.name == "" || newuser.storename == undefined || newuser.storename == "" || newuser.address1 == undefined || newuser.address1 == "" || newuser.address2 == undefined || newuser.address2 == "" || newuser.city == undefined || newuser.city == "" || newuser.state == undefined || newuser.state == "" || newuser.zipcode == undefined || newuser.zipcode == "" || newuser.workphone == undefined || newuser.workphone == "" || newuser.pancard == undefined || newuser.pancard == "") {
                                        alert("Fields are required");
                                    } else {
                                        $.getJSON(API_URL + "user-create?username=" + newuser.username + "&password=" + newuser.password + "&name=" + newuser.name + "&email=" + newuser.email + "&mobile=" + newuser.mobile + "&address1=" + newuser.address1 + "&address2=" + newuser.address2 + "&city=" + newuser.city + "&state=" + newuser.state + "&zip=" + newuser.zipcode + "&work_phone=" + newuser.workphone + "&store_name=" + newuser.storename + "&pancard=" + newuser.pancard, function (data1) {
                                            console.log(data1);
                                            $state.go('register');
                                        });
                                    }
                                }
                            };
                        })
                    
                        .controller('HomeTabCtrl', function ($scope, $state) {
                              
                            //  console.log('HomeTabCtrl');
                            $scope.items = [];
                            $scope.searchall = "";
                            var its = [];
                            $.getJSON(API_URL + "product-categories?auth=" + user_id, function (data) {
                                if (data.status == true) {
                                    for (var i = 0; i < data.categories.length; i++) {
                                        its.push(data.categories[i]);
                                    }
                                    $scope.items = its;                                      
                                    $state.transitionTo($state.current, null, { reload: true, inherit: true, notify: true });
                                }
                              
                            });
                            $scope.clearSearch = function () {
                              
                                search_product = $(".searchall").val();
                                location.href = "#/tab/products";
                            };                         
                            $scope.showProducts = function (category) {
                                category_selected = category;
                                location.href = "#/tab/products";
                            };
                        })

                     
                        
                        .controller('ProductsCtrl', function ($scope,$state) {

                            $scope.products = [];                         
                            var product_list = [];

                            $scope.showproductdetails = function (productid) {
                                selected_product = productid;
                                location.href = '#/tab/productdetails';
                            };                           
                            if (search_product != "") {                              
                                $.getJSON(API_URL + "product-search?keyword=" + search_product + "&auth=" + user_id, function (data) {
                                    if (data.status == true) {
                                        for (var i = 0; i < data.products.length; i++) {
                                            product_list.push(data.products[i]);
                                        }
                                        $scope.products = product_list;   
                                        $state.transitionTo($state.current, null, { reload: true, inherit: true, notify: true });
                                    }                                  
                                  });
                            } else {
                                $.getJSON(API_URL + "products-all?category=" + category_selected + "&auth=" + user_id, function (data) {
                                    console.log(data);                                    
                                    if (data.status == true) {
                                        for (var i = 0; i < data.products.length; i++) {
                                            product_list.push(data.products[i]);
                                        }
                                        $scope.products = product_list;                                                                             
                                        $state.transitionTo($state.current, null, { reload: true, inherit: true, notify: true });
                                    }
                                });
                            }
                             $scope.productSearch = function (){
                                  
                                search_product = $(".productsearchall").val();
                                var products_list = [];  
                                if (search_product != "") {  
                                 $.getJSON(API_URL + "product-search?keyword=" + search_product + "&auth=" + user_id, function (data) {
                                    if (data.status == true) {
                                        for (var i = 0; i < data.products.length; i++) {
                                            products_list.push(data.products[i]);
                                        }
                                        $scope.products = products_list;  
                                        $state.transitionTo($state.current, null, { reload: true, inherit: true, notify: true });
                                        
                                    }
                                });
                                  
                            }
                           };

                        })

                        .controller('ProductDetailsCtrl', function ($scope, $state) {
                            $scope.product = "";
                            var productdetails = [];

                            $.getJSON(API_URL + "product-detail?id=" + selected_product + "&auth=" + user_id, function (data) {
                                console.log(data);
                                if (data.status == true) {
                                    $scope.product = data.product;         
                                    console.log($scope.product);                                   
                                }
                                $state.transitionTo($state.current, null, { reload: true, inherit: true, notify: true });
                            });
                       

                            $scope.mycart = function (productid, title, price) {                                
          
                                var matchFound = false;
                                for (var i = 0; i < my_cart.length; i++) {
                                    if (my_cart[i].product_id == productid) {
                                        matchFound = true;
                                        break;
                                    }
                                }
                                if (matchFound == false) {
                                    var objData = {
                                        'product_id': productid,
                                        'quantity': 0,
                                        'price': price,
                                        'title': title
                                    };
                                  
                                    my_cart.push(objData);
                                    console.log(my_cart);                                    
                         
                                }
                                location.href = '#/tab/mycart';
                            };

                        })

                        .controller('MyCartCtrl', function ($scope) {
                                                 
                          $('#my_cart').css("display","block");
                  
                            console.log(my_cart);
                            $scope.cart = my_cart;
                            $scope.product = "";
                            $scope.cartTotal = 0;
                            
                          

                            $scope.removeButton = function (index) {
                                for (var i = 0; i < my_cart.length; i++) {
                                    if (my_cart[i].product_id == index) {
                                        my_cart.splice(i, 1);
                                    }
                                }
                                $(".product_" + index).hide();   
                                $(".final_total").empty();
                              
                            };
                            //$scope.checkout = function(){
                            //  location.href = '#/tab/myaccount';
                            //};
                            $scope.quantities = [
                                {id: 1, value: 1},
                                {id: 2, value: 2},
                                {id: 3, value: 3},
                                {id: 4, value: 4},
                                {id: 5, value: 5},
                                {id: 6, value: 6},
                                {id: 7, value: 7},
                                {id: 8, value: 8},
                                {id: 9, value: 9},
                                {id: 10, value: 10}
                            ];

                            $scope.onQuantityChange = function (itemSel, pprice, pid) {
                               
                                $(".product_" + pid + " .total").html(($scope.itemSelected.value * pprice).toFixed(2));
                                var finalTotal = 0;
                                $(".cart_products").each(function () {
                                    finalTotal += parseFloat($(this).find(".total").html());
                                });
                                
                                for (var i=0;i<my_cart.length;i++) {
                                    if (pid == my_cart[i].product_id) {
                                        my_cart[i].quantity = $scope.itemSelected.value;
                                    }
                                }
                                $(".final_total").show();
                                $(".final_total").html(finalTotal.toFixed(2));
                            };

                            $scope.confirmOrder = function () {
                                var orderString = "order-create?auth=" + user_id + "&";
                                for (var i=0;i<my_cart.length;i++) {
                                    orderString += "&product_id[]=" + my_cart[i].product_id;
                                    orderString += "&quantity[]=" + my_cart[i].quantity;
                                    orderString += "&price[]=" + my_cart[i].price;
                                }
                               
                                        

                                $.getJSON(API_URL + orderString, function (data) {                                                                    
                                   
                                    if (data.status == true) {                                      
                                        alert(data.message);  
                                        my_cart.length = 0;
                                        $('#my_cart').css("display","none");
                                        $('.final_total').empty();
                                        location.href = '#/tab/myaccount';  
                                        
                                    }                                
                               
                                });

                            }


                        }).directive('ngConfirmClick', [
                    function () {
                        return {
                            link: function (scope, element, attr) {
                                var msg = attr.ngConfirmClick || "Are you sure?";
                                var clickAction = attr.confirmedClick;
                                element.bind('click', function (event) {
                                    var zeroQuantity = false;
                                    var noOfProducts = my_cart.length;
                                    for (var i=0;i<my_cart.length;i++) {
                                        if (my_cart[i].quantity == 0) {
                                            zeroQuantity = true;
                                            break;
                                        }
                                    }

                                    if (noOfProducts > 0) {
                                        if (zeroQuantity == true) {
                                            alert("One or more of your items have 0 quantity. Please select quantity or remove product");
                                        } else {
                                            console.log(my_cart);
                                            if (window.confirm(msg)) {
                                                scope.$eval(clickAction)
                                                    
                                            }                                         
                                        }
                                    } else {
                                        alert("You have not selected any products. Please browse our products and add them to your cart before you can checkout.")
                                    }
                                });
                            }
                        };
                    }])
                    .controller('TrackOrderCtrl', function ($scope, $state) {
                        
                           
                           
                            $scope.orders = "";
                            var trackorderdetails = [];

                            $.getJSON(API_URL + "order-track?auth=" + user_id , function(data){
                            console.log(data);
                            if(data.status == true){
                                 for (var i = 0; i < data.orders.length; i++) {
                                        data.orders[i].ordered_at =  new Date(data.orders[i].ordered_at)
                                        trackorderdetails.push(data.orders[i]);
                                         
                                        }                                
                                $scope.orders = trackorderdetails;
                                console.log($scope.orders);                           
                                                           
                                }
                              $state.transitionTo($state.current, null, { reload: true, inherit: true, notify: true }); 

                            });
                            $scope.orderdetail = function(order){
                                selected_ordered = order;
                                location.href = '#/tab/orderdetail';
                                 
                        };
                       
                    })



                    .controller('OrderdetailCtrl', function ($scope, $state){
                        $scope.cartTotal = 0;
//                        alert("Ordered ID : "+ selected_ordered);
                              
                             $scope.orderdetails = "";
                             var orderdetails = [];

                             $.getJSON(API_URL + "order-detail?id=" + selected_ordered + "&auth=" + user_id, function(data){
                             console.log(data);
                             if(data.status = true){
                                $scope.orderdetails = data.order_detail;
                                var total = 0;
                                console.log($scope.orderdetails);
                                for (var i=0;i<data.order_detail.length;i++) {
                                    total = total + (data.order_detail[i].quantity * data.order_detail[i].price)
                                }
                                $scope.cartTotal = total;
                                $state.transitionTo($state.current, null, { reload: true, inherit: true, notify: true });
                             }
                                
                            });

                    })